import Vue from 'vue'
import ProjectItem from '@/components/ProjectItem.vue'
import sampleProject from './sampleProject.json'

function createSampleProjectItem () {
  const constructor = Vue.extend(ProjectItem)
  const vm = new constructor({propsData: {project: sampleProject}}).$mount()
  return vm
}

describe('ProjectItem.vue', () => {
  it('should display correct project name', () => {
    const vm = createSampleProjectItem()

    expect(vm.$el.querySelector('.ProjectItem-projectName').textContent)
      .to.equal('Heat sink-Electronics cooling using CHT')
  })

  it('should display correct author', () => {
    const vm = createSampleProjectItem()

    expect(vm.$el.querySelector('.ProjectItem-ownerName').textContent)
      .to.equal('by sjesu_rajendra')
  })

  it('should display correct image', () => {
    const vm = createSampleProjectItem()

    expect(vm.$el.querySelector('.ProjectItem-image').getAttribute('style'))
      .to.contain('https://www.simscale.com/api/v1/projects/sjesu_rajendra/heat_sink-electronics_cooling/153d2392-9ca3-4b90-ae34-662bbfba4888/thumbnail?token=410138f260188c5ec7b82d3b7d0c807004b94ccb42c64265476060d4a45de6d420170310T115000.000Z')
  })

  it('should display correct statistics', () => {
    const vm = createSampleProjectItem()

    expect(vm.$el.querySelector('[title="copies"]').textContent)
      .to.equal('135')
    expect(vm.$el.querySelector('[title="likes"]').textContent)
      .to.equal('46')
    expect(vm.$el.querySelector('[title="views"]').textContent)
      .to.equal('2140')
  })
})
