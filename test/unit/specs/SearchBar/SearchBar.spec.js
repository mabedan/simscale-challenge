import Vue from 'vue'
import SearchBar from '@/components/SearchBar.vue'
import $ from 'jquery'
import sinon from 'sinon'

function createSearchBar (propsData) {
  const constructor = Vue.extend(SearchBar)
  const vm = new constructor({ propsData }).$mount()
  return vm
}

describe('SearchBar.vue', () => {
  it('should the given query at creation', () => {
    const vm = createSearchBar({query: 'test'})
    expect(vm.$el.querySelector('input').value).to.equal('test')
  })

  xit('should store user input in enteredQuery', (done) => {
    const vm = createSearchBar({query: ''})
    const input = vm.$el.querySelector('input')

    $(input).val('myTest').trigger('change') // didn't work out...
    Vue.nextTick(() => {
      $(vm.$el.querySelector('button')).click()
      expect(vm.enteredQuery).to.equal('myTest')

      done()
    })
  })

  it('should update ui based on model change', (done) => {
    const vm = createSearchBar({query: ''})

    vm.enteredQuery = 'myTest'
    Vue.nextTick(() => {
      expect(vm.$el.querySelector('input').value).to.equal('myTest')

      done()
    })
  })

  it('should emit event on user click, providing user input', (done) => {
    const vm = createSearchBar({query: ''})
    const spy = sinon.spy()

    vm.$on('commitSearch', spy)
    vm.enteredQuery = 'myTest'

    Vue.nextTick(() => {
      $(vm.$el.querySelector('button')).click()
      expect(spy.calledWith('myTest')).to.be.true

      done()
    })
  })
})
