import Vue from 'vue'
import ProjectList from '@/components/ProjectList.vue'
import sampleProjects from './sampleProjects.json' // contains 2 items
import secondarySampleProjects from './SecondarySampleProjects.json' // contains 3 items
import sinon from 'sinon'
import {projectsApi as api} from '@/data/SimscaleApi'

function createProjectList () {
  const constructor = Vue.extend(ProjectList)
  const vm = new constructor().$mount()
  return vm
}
function tick (callback, times = 2) {
  Vue.nextTick(() => {
    if (times === 1) {
      callback()
    } else {
      tick(callback, times - 1)
    }
  })
  // for some reason needed more than one tick
}
describe('ProjectList.vue', () => {
  let sandbox
  beforeEach(() => { sandbox = sinon.sandbox.create() })
  afterEach(() => sandbox.restore())

  it('should display error if loading fails', (done) => {
    sandbox.stub(api, 'get').rejects(null)

    const vm = createProjectList()
    tick(() => {
      expect(vm.$el.querySelector('.ProjectList-prompt').textContent)
        .to.contain('An error occurred')

      done()
    })
  })

  it('should display error if recieves error from server', (done) => {
    sandbox.stub(api, 'get').resolves({status: 500})

    const vm = createProjectList()
    tick(() => {
      expect(vm.$el.querySelector('.ProjectList-prompt').textContent)
        .to.contain('An error occurred')

      done()
    })
  })

  it('should display loading message while loading', (done) => {
    let delayedResolveCallback
    const resolved = new Promise((resolve, reject) => {
      delayedResolveCallback = resolve
    })
    sandbox.stub(api, 'get').returns(resolved)
    const vm = createProjectList()
    tick(() => {
      expect(vm.$el.querySelector('.ProjectList-prompt').textContent)
        .to.contain('Loading')
      delayedResolveCallback({data: sampleProjects, status: 200})
      tick(() => {
        expect(vm.$el.querySelector('.ProjectList-prompt')).to.equal(null)
        done()
      })
    })
  })

  it('should not display prompt message if data delivered', (done) => {
    const resolved = new Promise((resolve, reject) => {
      resolve({data: sampleProjects, status: 200})
    })
    sandbox.stub(api, 'get').returns(resolved)
    const vm = createProjectList()
    tick(() => {
      expect(vm.$el.querySelector('.ProjectList-prompt'))
        .to.equal(null)
      done()
    })
  })

  it('should display correct number of items', (done) => {
    sandbox.stub(api, 'get').resolves({data: sampleProjects, status: 200})
    const vm = createProjectList()
    tick(() => {
      const q = vm.$el.querySelectorAll('.ProjectItem')
      expect(q.length)
        .to.equal(2)
      expect(vm.$el.querySelector('.ProjectItem:first-child').textContent)
        .to.contain('Aerodynamics analysis of a Formula One F1 Race car')
      done()
    })
  })

  it('should read the correct filter from query, when the route changes', (done) => {
    const stub = sandbox.stub(api, 'get').resolves({data: sampleProjects, status: 200})
    const vm = createProjectList()
    tick(() => {
      vm.stateChange({query: {projectName: 'testquery'}})
      tick(() => {
        expect(stub.getCall(1).args[1].params.filter).to.equal('testquery')
        done()
      })
    })
  })

  it('should react to reroute correctly, and show the new items', (done) => {
    sandbox.stub(api, 'get')
      .onCall(0).resolves({data: sampleProjects, status: 200})
      .onCall(1).resolves({data: secondarySampleProjects, status: 200})

    const vm = createProjectList()
    tick(() => {
      vm.stateChange({query: {projectName: 'test'}})
      tick(() => {
        const q = vm.$el.querySelectorAll('.ProjectItem')
        expect(q.length)
          .to.equal(3)
        expect(vm.$el.querySelector('.ProjectItem:nth-child(2)').textContent)
          .to.contain('Centrifugal') // a word in the second item of "secondarySampleProjects"
        done()
      }, 3)
    })
  })

  it('should show failure message and existing items, if the request following the reroute fails', (done) => {
    sandbox.stub(api, 'get')
      .onCall(0).resolves({data: sampleProjects, status: 200})
      .onCall(1).rejects(null)

    const vm = createProjectList()
    tick(() => {
      vm.stateChange({query: {projectName: 'test'}})
      tick(() => {
        const q = vm.$el.querySelectorAll('.ProjectItem')
        expect(q.length)
          .to.equal(2)
        expect(vm.$el.querySelector('.ProjectList-prompt').textContent)
          .to.contain('An error occurred')
        done()
      }, 3)
    })
  })

  it('should show loading indicator, if the request following the reroute is in progress', (done) => {
    let delayedResolveCallback
    const resolved = new Promise((resolve, reject) => {
      delayedResolveCallback = resolve
    })
    sandbox.stub(api, 'get')
      .onCall(0).resolves({data: sampleProjects, status: 200})
      .onCall(1).returns(resolved)

    const vm = createProjectList()
    tick(() => {
      vm.stateChange({query: {projectName: 'test'}})
      tick(() => {
        expect(vm.$el.querySelector('.ProjectList-prompt').textContent)
          .to.contain('Loading')
        delayedResolveCallback({data: secondarySampleProjects, status: 200})
        tick(() => {
          const q = vm.$el.querySelectorAll('.ProjectItem')
          expect(q.length)
            .to.equal(3)
          expect(vm.$el.querySelector('.ProjectItem:nth-child(2)').textContent)
            .to.contain('Centrifugal') // a word in the second item of "secondarySampleProjects"

          done()
        })
      }, 3 /* I know... */)
    })
  })

  /**
   * a lot more tests can be added...
   */
}, 5000)
