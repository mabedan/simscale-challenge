import routingHelper from '@/router/routingHelper.js'
import sinon from 'sinon'

describe('routingHelper', () => {
  it('should read the query in the route object', () => {
    expect(routingHelper.readListViewSearchQuery({query: {projectName: 'test'}}))
      .to.equal('test')
  })
  it('should return empty string if the route object is incomplete', () => {
    expect(routingHelper.readListViewSearchQuery(null))
      .to.equal('')
    expect(routingHelper.readListViewSearchQuery({}))
      .to.equal('')
    expect(routingHelper.readListViewSearchQuery({query: {}}))
      .to.equal('')
  })
  it('should set the query in the route object', () => {
    const spy = sinon.spy()

    routingHelper.pushListViewSearchQuery({push: spy}, 'test')
    expect(spy.calledWith({query: {projectName: 'test'}}))
      .to.be.true
  })
  it('should handle gracefully if route object passed to the pushListViewSearchQuery function is incomplete', () => {
    expect(() => routingHelper.pushListViewSearchQuery({}, 'test'))
      .not.to.throw()
    expect(() => routingHelper.pushListViewSearchQuery({query: 'not a function'}, 'test'))
      .not.to.throw()
    expect(() => routingHelper.pushListViewSearchQuery(null, 'test'))
      .not.to.throw()
  })
})
