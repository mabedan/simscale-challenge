import axios from 'axios'
const baseDomain = 'https://www.simscale.com'
const baseURL = `${baseDomain}/api/v1/projects`

// used for managing communication with simscale api
const projectsApi = axios.create({
  baseURL: baseURL,
  params: {
    limit: 40,
    offset: 0,
    order: 'desc',
    filterby: 'project_text',
    sortby: 'project_likes'
  }
})
export {
  baseDomain,
  baseURL,
  projectsApi
}
