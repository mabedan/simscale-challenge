const locales = {
  en: {
    retry: 'try again',
    loading: 'Loading...',
    networkError: 'An error occurred while loading projects',
    networkErrorForProjectName: `An error occurred while loading projects named "{name}"`,
    imageMissing: 'No Preview Image',

    cadsCount: '{count} CAD|{count} CAD models',
    meshesCount: '{count} Simulation|{count} Simulations',
    simulationsCount: '{count} Mesh|{count} Meshes',
    authorAttribution: 'by {author}',
    changedDate: 'Changed ',

    projectsTitle: 'Public Projects',

    likes: 'likes',
    views: 'views',
    copies: 'copies'
  },
  fr: {
    retry: 'réessayez',
    loading: 'En Cours de Chargement...',
    networkError: `Une erreur s'est produite lors de telechargement`,
    networkErrorForProjectName: `Une erreur s'est produite lors de telechargement des projects ayant le nom " "{name}"`,
    imageMissing: 'Sans Aperçu',

    cadsCount: '{count} CAD|{count} CADs',
    meshesCount: '{count} simulation|{count} simulations',
    simulationsCount: '{count} engrenage|{count} engrenages',
    authorAttribution: 'par {author}',
    changedDate: 'Modifié ',

    projectsTitle: 'Projects Publiques',

    likes: 'favoris',
    views: 'consultation',
    copies: 'copis'
  }
}

export default locales
