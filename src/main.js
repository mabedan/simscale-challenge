// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from '@/router/router'
import VueI18n from 'vue-i18n'
import locales from '@/i18n/locales'
import VueMoment from 'vue-moment'
require('@/style/font-icons.scss')

Vue.config.productionTip = false
const locale = 'en'// @todo find better way to setup config
Vue.config.lang = locale

// set up localisation
Vue.use(VueI18n)
Object.keys(locales).forEach(function (lang) {
  Vue.locale(lang, locales[lang])
})

// set up date formatting module
Vue.use(VueMoment)
require(`moment/locale/${locale === 'en' ? 'en-gb' : locale}`)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
