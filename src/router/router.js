import Vue from 'vue'
import Router from 'vue-router'
import ProjectList from '@/components/ProjectList'
import ProjectListHeader from '@/components/ProjectListHeader'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ProjectList',

      // Place menu and project list, into their designated places.
      // refer to the <router-view> elements inside template of app.vue
      components: {
        content: ProjectList,
        menu: ProjectListHeader
      }
    }
  ]
})
