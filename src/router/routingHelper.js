const projectNameQueryParameter = 'projectName'
function readListViewSearchQuery (route) { // @todo better naming?
  if (!route || !route.query) {
    return ''
  }
  return route.query[projectNameQueryParameter] || ''
}
function pushListViewSearchQuery (route, queryString) {
  if (!route || (typeof route.push) !== 'function') {
    return
  }
  let query = {}
  query[projectNameQueryParameter] = queryString
  return route.push({query})
}

export default {
  readListViewSearchQuery,
  pushListViewSearchQuery
}
