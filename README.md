# Simscale Assessment Case

Mehdi Abedanzadeh Esfahani

### Points of interest

- I decided to use ```Vue.js```. I've never use this framework before, but I was curious to give it a go and this project seemed like a good opportunity. The main thing to know is that ui components are written in ```.vue``` format, which is simply a file, composed of html, script and style.
- Entry point is the ```main.js``` file.
- I'm follwing the [BEM](http://getbem.com/introduction/) method for organizing my css. This method avoids extra work for browser by not using cascading rules.
- Project is built from the [vue.js webpack template](https://github.com/vuejs-templates/webpack).
- Initially I used a classic css grid layout, which I generated via https://purecss.io/start/#build-your-pure-starter-kit using the dimentions used in your website, but eventually decided against it given the small scope of this project.
- The choice of the screen widths (```1200px```, ```992px```, ```768px```) for number of items per row (4, 3, 2) seemed odd to me. The reason being that it causes smaller screens to have bigger items, which ultimately causes less number of rows visible at a time.
My personal suggestion would be to choose a fixed item width, and deducing the different screen widths from that. For example ```300px``` item width (including padding), and consequently ```1200```, ```900```, and ```600``` for screen widths, maintaining a fixed height per item. Up for discussion.
Another solution is not to keep the image holder aspect ratio the same in all cases, by having a fixed height for items in all screen sizes.
- Personally I suggest against using hover effects in lists, unless they're absolutely necessary. The spec uses ```box-shadow``` as hover effect, which is a cpu operation (repaints the div at each frame completely). The shifting effect of the cards on the other hand, is less problematic given that it can be handled via gpu via transform css property.
- In general ```box-shadows```, ```corner-radius``` and ```gradient``` hinders performance on lower-end devices. A solution is to use prepared pngs (for different screen resolutions) for those elements.
- In some cases where parts of the spec seemed a little bit off in the .psd, I went with common sense. In normal circumstances I would simply provide feedback to PM/PO and do as they suggest. example:
  - The horizontal spacing between items in was not consistent, sometimes more than ```30px```, sometimes less. I used ```30px```.
  - The "Public Projects" title was not aligned to the left of the items.

### Known Issues

- Entering the mouse from below the item, will cause the item to "jump", becoming a moving target.
- The eye icon is not the same as in the spec.
- The heart icon is slightly larger in comparison with other icons.
- moment.js includes all of the locales into the final js file, which takes up more than 50%.
- The unit tests don't pass on PhantomJS, because the axios library which I use for networking is Promise based and PhantomJS doens't support native Promise.
- Mixed up use of single/double quotes in the template section.
- Not all of the *dev* dependencies in package.json are used.

### Skipped topics

- SEO. Although Vue.js provides easy functionality for moving to server-side rendering.
- the very top header and the footer.
- Automating the svg->font operation.
- Funcionality of the sort buttons. An instance of my implementation for parent/child communication is the mechanism between the ProjectHeaderList and SearchBar components.
- Unit tests for ```ProjectListHeader```, ```IconButton``` and ```IconStat```.
- Where you see ```@todo```.

## Build Setup

``` bash
# tested primarily on mac

# installs npm modules
# runs the tests
# builds for prod, into ./dist folder
# starts webserver SimpleHTTPServer
sh simscale.sh
```
